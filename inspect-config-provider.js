//
// inspect config provider
//
// Use docker inspect to detect node configuration
//

const { exec, spawn } = require('child_process');
const EventEmitter = require('events');
const readline = require('readline');

const emitter = new EventEmitter();

const getCommand = ids => `docker inspect -f '"{{.Id}}": { "env": {{json .Config.Env }}, "isRunning": {{json .State.Running}} },' ${ids}`;

const get = ( eventName='update', ids='$(docker ps -aq)', cb=()=>{} ) => {
  const promise = new Promise((resolve, reject) => {
    exec(getCommand(ids), (error, stdout, stderr) => {
      if (error) {
        const err = new Error(`Docker inspect error: ${error}`);
        console.error(err.message);
        return reject(err);
      }
  
      let containers;
      try {
        containers = JSON.parse(`{ ${stdout.substr(0, stdout.length - 2)} }`);
      }
      catch(e) {
        const err = new Error(`Failed to parse inspect result: ${stdout}`);
        console.error(err.message);
        return reject(err);
      }
  
      console.log('[inspect-config-provider] containers >>>', containers);
  
      let config = { repos: {} };
      Object.keys(containers).forEach((container) => {
        const { env, isRunning } = containers[container];
  
        let vars = {};
        env.forEach((item) =>  {
          const parts = item.split('=');
          vars[parts[0]] = parts[1];
        });
  
        if ((isRunning && vars.GIT_REPO && vars.HOOKUP_ROUTE) ||
            (!isRunning && vars.GIT_REPO && vars.HOOKUP_ROUTE && vars.HOOKUP_NO_EXECUTOR)) {
          const gitRepoParts = vars.GIT_REPO.split('/');
          const repoName = gitRepoParts.pop();
          
          if (!repoName) return;
  
          const branch = vars.GIT_BRANCH || 'master';
          const workdir = vars.HOOKUP_WORKDIR || `${repoName && repoName.replace('.git', '')}_${branch}`;
          const name = `${workdir}_${vars.SERVICE_ID}`;
  
          const repo = {
            url: vars.GIT_REPO,
            route: vars.HOOKUP_ROUTE,
            containerId: container,
            serviceId: vars.SERVICE_ID,
            executor: vars.HOOKUP_NO_EXECUTOR ?
              false :
              (vars.HOOKUP_EXECUTOR ? { type: vars.HOOKUP_EXECUTOR } : { type: 'hookup-compose-executor' }),
            branch,
            workdir,
            packageInstaller: vars.HOOKUP_PACKAGE_INSTALLER || 'yarn',
            isPassive: !!vars.HOOKUP_NO_EXECUTOR,
            env: vars,
          };
  
          config.repos[name] = repo;
        }
      });
  
      resolve(config);
    });
  });

  promise
    .then((config) => {
      cb(null, config);
      emitter.emit(eventName, config);
    })
    .catch(cb);

  return promise;
};

process.on('SIGHUP', () => {
  console.log('Reloading config ...');
  get();
});

const listenToContainers = () => {
  const docker = spawn(
    'docker',
    [ 'events', '--filter="event=start"', '--filter="event=stop"', '--format', '"{{json .}}"']);

  const rl = readline.createInterface({
    input: docker.stdout,
  });

  rl.on('line', (line) => {
    const clearline = line.substr(1,line.length - 2);
    try {
      const event = JSON.parse(clearline);
      if (event.Type === 'container') {
        if (event.Action === 'start') {
          get('addRepo', event.id);
        }
        else if (event.Action === 'destroy') {
          emitter.emit('removeRepo', { containerId: event.id })
        }
      }
    }
    catch(e) {
      console.error(`Failed to parse docker event: ${clearline}`);
    }
  });

  return docker;
};

emitter.get = get;

module.exports = emitter;

(function startListener() {
  console.log('Starting docker listener');
  const docker = listenToContainers();
  docker.on('close', (code) => {
    if (code !== 0) {
      console.log(`docker process exited with code ${code}`);
    }
    setTimeout(() => startListener(), 1000);
  });
})();

